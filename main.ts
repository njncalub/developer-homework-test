import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import { NutrientFact, Product, Recipe } from "./supporting-files/models";
import {
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

/** Shows the cheapest cost for a recipe, and the nutrients at that cost. */
interface CheapestCostSummary {
  cheapestCost: number;
  nutrientsAtCheapestCost: Record<string, NutrientFact>;
}

/**
 * Finds the cheapest product from a list of Products. It will return undefined
 * if the list of Products is empty.
 *
 * @param { Product[] } products The list of Products to search.
 * @returns { Product | undefined } The cheapest Product, or undefined if the
 * list of Products is empty.
 */
const getCheapestProduct = (products: Product[]): Product | undefined => {
  if (!products || products.length === 0) {
    return undefined;
  }

  let cheapestProduct: Product | undefined;
  let cheapestPrice: number | undefined;

  for (const product of products) {
    if (!product.supplierProducts || product.supplierProducts.length === 0) {
      continue;
    }

    for (const supplierProduct of product.supplierProducts) {
      const costPerBaseUnit = GetCostPerBaseUnit(supplierProduct);

      if (cheapestPrice === undefined || costPerBaseUnit < cheapestPrice) {
        cheapestProduct = product;
        cheapestPrice = costPerBaseUnit;
      }
    }
  }

  return cheapestProduct;
};

/**
 * Extracts the cheapest cost and nutrients at that cost for a recipe. If the
 * recipe is undefined, or has no line items, then undefined is returned.
 *
 * @param { Recipe } recipe The recipe to calculate the cheapest cost for.
 * @returns { CheapestCostSummary | undefined } The cheapest cost and nutrients
 * at that cost for the recipe, or undefined if there are no line items in the
 * recipe.
 */
const getCheapestCostSummary = (
  recipe: Recipe
): CheapestCostSummary | undefined => {
  if (!recipe || !recipe.lineItems || recipe.lineItems.length === 0) {
    return undefined;
  }

  let totalCost = 0;
  const totalNutrients: Record<string, NutrientFact> = {};

  for (const lineItem of recipe.lineItems) {
    const { ingredient } = lineItem;
    const products = GetProductsForIngredient(ingredient);

    const cheapestProduct = getCheapestProduct(products);
    if (!cheapestProduct) {
      continue;
    }

    // We want to find the cheapest supplier product by using the cost per base
    // unit.
    const cheapestSupplierProduct = cheapestProduct.supplierProducts.reduce(
      (cheapest, supplierProduct) => {
        const currentCost = GetCostPerBaseUnit(supplierProduct);
        const cheapestCost = GetCostPerBaseUnit(cheapest);
        return currentCost < cheapestCost ? supplierProduct : cheapest;
      }
    );

    // The cost of the ingredient is the cost per base unit multiplied by the
    // amount of the ingredient in the recipe.
    const ingredientCost =
      GetCostPerBaseUnit(cheapestSupplierProduct) *
      lineItem.unitOfMeasure.uomAmount;
    totalCost += ingredientCost;

    // Now, we need to add the nutrient facts for the ingredient to the total
    // nutrient facts for the recipe. We need to convert the nutrient facts to
    // base units first and then add them to the total nutrient facts.
    for (const nutrientFact of cheapestProduct.nutrientFacts) {
      const baseNutrientFact = GetNutrientFactInBaseUnits(nutrientFact);

      if (totalNutrients[baseNutrientFact.nutrientName]) {
        totalNutrients[
          baseNutrientFact.nutrientName
        ].quantityAmount.uomAmount += baseNutrientFact.quantityAmount.uomAmount;
      } else {
        totalNutrients[baseNutrientFact.nutrientName] = baseNutrientFact;
      }
    }
  }

  // Since the RunTest function is looking for a sorted totalNutrients data, we
  // need to convert the object into an array, sort it, and then convert it back
  // into an object.
  const sortedTotalNutrientsArray = Object.values(totalNutrients).sort((a, b) =>
    a.nutrientName.localeCompare(b.nutrientName)
  );
  const sortedTotalNutrients: Record<string, NutrientFact> = {};
  for (const nutrientFact of sortedTotalNutrientsArray) {
    sortedTotalNutrients[nutrientFact.nutrientName] = nutrientFact;
  }

  return {
    cheapestCost: totalCost,
    nutrientsAtCheapestCost: sortedTotalNutrients,
  };
};

recipeData.forEach((recipe) => {
  recipeSummary[recipe.recipeName] = getCheapestCostSummary(recipe);
});

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
